#include "openamp.h"
#include "virt_uart.h"
#include "openamp_log.h"

#define MSG_TIME "*time"
#define MSG_LED_ON "*led_on"
#define MSG_LED_OFF "*led_off"
#define MSG_CLR_BUF "*clear"
#define MSG_DELAY "*delay"

#define MAX_BUFFER_SIZE RPMSG_BUFFER_SIZE

extern bool OpenAmpIsInit;

extern VIRT_UART_HandleTypeDef hvirtuart0;
extern VIRT_UART_HandleTypeDef hvirtuart1;

extern __IO FlagStatus VirtUart0RxMsg;
extern uint8_t VirtUart0ChannelBuffRx[MAX_BUFFER_SIZE];
extern uint8_t VirtUart0ChannelBuffTx[MAX_BUFFER_SIZE];
extern uint16_t VirtUart0ChannelRxSize;

extern __IO FlagStatus VirtUart1RxMsg;
extern uint8_t VirtUart1ChannelBuffRx[MAX_BUFFER_SIZE];
extern uint8_t VirtUart1ChannelBuffTx[MAX_BUFFER_SIZE];
extern uint16_t VirtUart1ChannelRxSize;


void User_VIRT_UART_Init(void);
void User_VIRT_UART0_Treatment(void);
void User_VIRT_UART1_Treatment(void);
void VIRT_UART0_RxCpltCallback(VIRT_UART_HandleTypeDef *huart);
void VIRT_UART1_RxCpltCallback(VIRT_UART_HandleTypeDef *huart);


enum { PREAMBLE=0xABCD, };
#pragma pack(1)
struct packet {
  uint16_t preamble;
  uint16_t length;
  uint16_t crc16;
};

typedef void (*VIRT_UART_RxCpltCallback)(VIRT_UART_HandleTypeDef *huart);

struct virt_uart {
  VIRT_UART_HandleTypeDef huart;
  __IO FlagStatus rx_status;
  __IO uint8_t *rx_buffer;
  __IO uint16_t rx_size;
  __IO FlagStatus tx_status;
  __IO uint8_t *tx_buffer;
  __IO uint16_t tx_size;
  VIRT_UART_RxCpltCallback cbk;
};

extern struct virt_uart virt_uart0;
extern __IO uint16_t virt_uart0_expected_nbytes;

extern struct virt_uart virt_uart1;
extern __IO uint16_t virt_uart1_expected_nbytes;


