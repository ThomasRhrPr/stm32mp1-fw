#include "openamp.h"
#include "virt_uart.h"
#include "openamp_log.h"

#define MSG_TIME "*time"
#define MSG_LED_ON "*led_on"
#define MSG_LED_OFF "*led_off"
#define MSG_DELAY "*delay"

#define MAX_BUFFER_SIZE RPMSG_BUFFER_SIZE

extern bool OpenAmpIsInit;

extern VIRT_UART_HandleTypeDef hvirtuart0;
extern VIRT_UART_HandleTypeDef hvirtuart1;

extern __IO FlagStatus VirtUart0RxMsg;
extern uint8_t VirtUart0ChannelBuffRx[MAX_BUFFER_SIZE];
extern uint8_t VirtUart0ChannelBuffTx[MAX_BUFFER_SIZE];
extern uint16_t VirtUart0ChannelRxSize;

extern __IO FlagStatus VirtUart1RxMsg;
extern uint8_t VirtUart1ChannelBuffRx[MAX_BUFFER_SIZE];
extern uint8_t VirtUart1ChannelBuffTx[MAX_BUFFER_SIZE];
extern uint16_t VirtUart1ChannelRxSize;


void User_VIRT_UART_Init(void);
void User_VIRT_UART0_Treatment(void);
void User_VIRT_UART1_Treatment(void);
void VIRT_UART0_RxCpltCallback(VIRT_UART_HandleTypeDef *huart);
void VIRT_UART1_RxCpltCallback(VIRT_UART_HandleTypeDef *huart);
