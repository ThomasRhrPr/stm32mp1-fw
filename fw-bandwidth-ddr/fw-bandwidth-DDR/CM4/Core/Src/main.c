/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2022 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "openamp.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "user_openamp.h"
#include "user_rpmsgDDR.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
IPCC_HandleTypeDef hipcc;

DMA_HandleTypeDef hdma_memtomem_dma2_stream1;
/* Definitions for TaskOpenamp */
osThreadId_t TaskOpenampHandle;
const osThreadAttr_t TaskOpenamp_attributes = {
		.name = "TaskOpenamp",
		.stack_size = 128 * 4,
		.priority = (osPriority_t) osPriorityRealtime,
};
/* Definitions for TaskHeartBeats */
osThreadId_t TaskHeartBeatsHandle;
const osThreadAttr_t TaskHeartBeats_attributes = {
		.name = "TaskHeartBeats",
		.stack_size = 128 * 4,
		.priority = (osPriority_t) osPriorityRealtime,
};
/* Definitions for TaskDDR */
osThreadId_t TaskDDRHandle;
const osThreadAttr_t TaskDDR_attributes = {
		.name = "TaskDDR",
		.stack_size = 128 * 4,
		.priority = (osPriority_t) osPriorityRealtime,
};
/* USER CODE BEGIN PV */
TickType_t msTime = 0;
unsigned long int OpenampCycle=0, ddrCycle = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void PeriphCommonClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_IPCC_Init(void);
int MX_OPENAMP_Init(int RPMsgRole, rpmsg_ns_bind_cb ns_bind_cb);
void StartTaskOpenamp(void *argument);
void StartTaskHeartBeats(void *argument);
void StartTaskDDR(void *argument);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void)
{
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	if(IS_ENGINEERING_BOOT_MODE())
	{
		/* Configure the system clock */
		SystemClock_Config();
	}

	if(IS_ENGINEERING_BOOT_MODE())
	{
		/* Configure the peripherals common clocks */
		PeriphCommonClock_Config();
	}
	else
	{
		/* IPCC initialisation */
		MX_IPCC_Init();
		/* OpenAmp initialisation ---------------------------------*/
		//MX_OPENAMP_Init(RPMSG_REMOTE, NULL);
	}

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	/* USER CODE BEGIN 2 */

	/* USER CODE END 2 */

	/* Init scheduler */
	osKernelInitialize();

	/* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */
	/* USER CODE END RTOS_MUTEX */

	/* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
	/* USER CODE END RTOS_SEMAPHORES */

	/* USER CODE BEGIN RTOS_TIMERS */
	/* start timers, add new ones, ... */
	/* USER CODE END RTOS_TIMERS */

	/* USER CODE BEGIN RTOS_QUEUES */
	/* add queues, ... */
	/* USER CODE END RTOS_QUEUES */

	/* Create the thread(s) */
	/* creation of TaskOpenamp */
	TaskOpenampHandle = osThreadNew(StartTaskOpenamp, NULL, &TaskOpenamp_attributes);

	/* creation of TaskHeartBeats */
	TaskHeartBeatsHandle = osThreadNew(StartTaskHeartBeats, NULL, &TaskHeartBeats_attributes);

	/* creation of TaskDDR */
	TaskDDRHandle = osThreadNew(StartTaskDDR, NULL, &TaskDDR_attributes);

	/* USER CODE BEGIN RTOS_THREADS */
	/* add threads, ... */
	/* USER CODE END RTOS_THREADS */

	/* USER CODE BEGIN RTOS_EVENTS */
	/* add events, ... */
	/* USER CODE END RTOS_EVENTS */

	/* Start scheduler */
	osKernelStart();

	/* We should never get here as control is now taken by the scheduler */
	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1)
	{
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

	/** Configure LSE Drive Capability
	 */
	HAL_PWR_EnableBkUpAccess();
	__HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_MEDIUMHIGH);

	/** Initializes the RCC Oscillators according to the specified parameters
	 * in the RCC_OscInitTypeDef structure.
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI
			|RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS_DIG;
	RCC_OscInitStruct.LSEState = RCC_LSE_ON;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = 16;
	RCC_OscInitStruct.HSIDivValue = RCC_HSI_DIV1;
	RCC_OscInitStruct.LSIState = RCC_LSI_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
	RCC_OscInitStruct.PLL2.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL2.PLLSource = RCC_PLL12SOURCE_HSE;
	RCC_OscInitStruct.PLL2.PLLM = 3;
	RCC_OscInitStruct.PLL2.PLLN = 66;
	RCC_OscInitStruct.PLL2.PLLP = 2;
	RCC_OscInitStruct.PLL2.PLLQ = 1;
	RCC_OscInitStruct.PLL2.PLLR = 1;
	RCC_OscInitStruct.PLL2.PLLFRACV = 0x1400;
	RCC_OscInitStruct.PLL2.PLLMODE = RCC_PLL_FRACTIONAL;
	RCC_OscInitStruct.PLL3.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL3.PLLSource = RCC_PLL3SOURCE_HSE;
	RCC_OscInitStruct.PLL3.PLLM = 2;
	RCC_OscInitStruct.PLL3.PLLN = 34;
	RCC_OscInitStruct.PLL3.PLLP = 2;
	RCC_OscInitStruct.PLL3.PLLQ = 17;
	RCC_OscInitStruct.PLL3.PLLR = 37;
	RCC_OscInitStruct.PLL3.PLLRGE = RCC_PLL3IFRANGE_1;
	RCC_OscInitStruct.PLL3.PLLFRACV = 6660;
	RCC_OscInitStruct.PLL3.PLLMODE = RCC_PLL_FRACTIONAL;
	RCC_OscInitStruct.PLL4.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL4.PLLSource = RCC_PLL4SOURCE_HSE;
	RCC_OscInitStruct.PLL4.PLLM = 4;
	RCC_OscInitStruct.PLL4.PLLN = 99;
	RCC_OscInitStruct.PLL4.PLLP = 6;
	RCC_OscInitStruct.PLL4.PLLQ = 8;
	RCC_OscInitStruct.PLL4.PLLR = 8;
	RCC_OscInitStruct.PLL4.PLLRGE = RCC_PLL4IFRANGE_0;
	RCC_OscInitStruct.PLL4.PLLFRACV = 0;
	RCC_OscInitStruct.PLL4.PLLMODE = RCC_PLL_INTEGER;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}

	/** RCC Clock Config
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_ACLK
			|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
			|RCC_CLOCKTYPE_PCLK3|RCC_CLOCKTYPE_PCLK4
			|RCC_CLOCKTYPE_PCLK5;
	RCC_ClkInitStruct.AXISSInit.AXI_Clock = RCC_AXISSOURCE_PLL2;
	RCC_ClkInitStruct.AXISSInit.AXI_Div = RCC_AXI_DIV1;
	RCC_ClkInitStruct.MCUInit.MCU_Clock = RCC_MCUSSOURCE_PLL3;
	RCC_ClkInitStruct.MCUInit.MCU_Div = RCC_MCU_DIV1;
	RCC_ClkInitStruct.APB4_Div = RCC_APB4_DIV2;
	RCC_ClkInitStruct.APB5_Div = RCC_APB5_DIV4;
	RCC_ClkInitStruct.APB1_Div = RCC_APB1_DIV2;
	RCC_ClkInitStruct.APB2_Div = RCC_APB2_DIV2;
	RCC_ClkInitStruct.APB3_Div = RCC_APB3_DIV2;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct) != HAL_OK)
	{
		Error_Handler();
	}

	/** Set the HSE division factor for RTC clock
	 */
	__HAL_RCC_RTC_HSEDIV(24);
}

/**
 * @brief Peripherals Common Clock Configuration
 * @retval None
 */
void PeriphCommonClock_Config(void)
{
	RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

	/** Initializes the common periph clock
	 */
	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_CKPER;
	PeriphClkInit.CkperClockSelection = RCC_CKPERCLKSOURCE_HSE;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	{
		Error_Handler();
	}
}

/**
 * @brief IPCC Initialization Function
 * @param None
 * @retval None
 */
static void MX_IPCC_Init(void)
{

	/* USER CODE BEGIN IPCC_Init 0 */

	/* USER CODE END IPCC_Init 0 */

	/* USER CODE BEGIN IPCC_Init 1 */

	/* USER CODE END IPCC_Init 1 */
	hipcc.Instance = IPCC;
	if (HAL_IPCC_Init(&hipcc) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN IPCC_Init 2 */

	/* USER CODE END IPCC_Init 2 */

}

/**
 * Enable DMA controller clock
 * Configure DMA for memory to memory transfers
 *   hdma_memtomem_dma2_stream1
 */
static void MX_DMA_Init(void)
{

	/* DMA controller clock enable */
	__HAL_RCC_DMAMUX_CLK_ENABLE();
	__HAL_RCC_DMA2_CLK_ENABLE();

	/* Configure DMA request hdma_memtomem_dma2_stream1 on DMA2_Stream1 */
	hdma_memtomem_dma2_stream1.Instance = DMA2_Stream1;
	hdma_memtomem_dma2_stream1.Init.Request = DMA_REQUEST_MEM2MEM;
	hdma_memtomem_dma2_stream1.Init.Direction = DMA_MEMORY_TO_MEMORY;
	hdma_memtomem_dma2_stream1.Init.PeriphInc = DMA_PINC_ENABLE;
	hdma_memtomem_dma2_stream1.Init.MemInc = DMA_MINC_ENABLE;
	hdma_memtomem_dma2_stream1.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
	hdma_memtomem_dma2_stream1.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
	hdma_memtomem_dma2_stream1.Init.Mode = DMA_NORMAL;
	hdma_memtomem_dma2_stream1.Init.Priority = DMA_PRIORITY_LOW;
	hdma_memtomem_dma2_stream1.Init.FIFOMode = DMA_FIFOMODE_ENABLE;
	hdma_memtomem_dma2_stream1.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_FULL;
	hdma_memtomem_dma2_stream1.Init.MemBurst = DMA_MBURST_INC4;
	hdma_memtomem_dma2_stream1.Init.PeriphBurst = DMA_PBURST_INC4;
	if (HAL_DMA_Init(&hdma_memtomem_dma2_stream1) != HAL_OK)
	{
		Error_Handler( );
	}

	/* DMA interrupt init */
	/* DMA2_Stream1_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA2_Stream1_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(DMA2_Stream1_IRQn);

}

/**
 * @brief GPIO Initialization Function
 * @param None
 * @retval None
 */
static void MX_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(PA13_GPIO_Port, PA13_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(PH7_GPIO_Port, PH7_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin : PA13_Pin */
	GPIO_InitStruct.Pin = PA13_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(PA13_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : PA14_Pin */
	GPIO_InitStruct.Pin = PA14_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(PA14_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : PH7_Pin */
	GPIO_InitStruct.Pin = PH7_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(PH7_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartTaskOpenamp */
/**
 * @brief  Function implementing the TaskOpenamp thread.
 * @param  argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartTaskOpenamp */
void StartTaskOpenamp(void *argument)
{
	/* USER CODE BEGIN 5 */
	/* Infinite loop */
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_13, 0);
	MX_OPENAMP_Init(RPMSG_REMOTE, NULL);
	User_VIRT_UART_Init();
	osThreadFlagsSet(TaskDDRHandle, 0x0001U);
	//User_HDR_Init();
	OpenAmpIsInit = true;
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_13, 1);
	/* Infinite loop */
	for(;;)
	{
		if(OpenAmpIsInit){
			OpenampCycle++;
			OPENAMP_check_for_message();

			if (VirtUart0RxMsg) {
				User_VIRT_UART0_Treatment();
			}

			if (VirtUart1RxMsg) {
				User_VIRT_UART1_Treatment();
			}
		}
	}
	/* USER CODE END 5 */
}

/* USER CODE BEGIN Header_StartTaskHeartBeats */
/**
 * @brief Function implementing the TaskHeartBeats thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartTaskHeartBeats */
void StartTaskHeartBeats(void *argument)
{
	/* USER CODE BEGIN StartTaskHeartBeats */
	static int toggleTimer=0;
	HAL_GPIO_WritePin(GPIOH, GPIO_PIN_7,1);
	/* Infinite loop */
	for(;;)
	{
		osDelay(10);
		msTime = xTaskGetTickCount(); //HAL_GetTick();
		toggleTimer+=10;
		if(toggleTimer>=500){
			HAL_GPIO_TogglePin(GPIOH, GPIO_PIN_7);
			toggleTimer=0;
		}
	}
	/* USER CODE END StartTaskHeartBeats */
}

/* USER CODE BEGIN Header_StartTaskDDR */
/**
 * @brief Function implementing the TaskDDR thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartTaskDDR */
void StartTaskDDR(void *argument)
{
	/* USER CODE BEGIN StartTaskDDR */
	osThreadFlagsWait(0x0001U, osFlagsWaitAny, osWaitForever);
	User_HDR_Init();
	//static char buffer[100]={'\0'}; static int i=0;

	//int nbReadBuff=0;
	//static int bufferid = 1;
	/* Infinite loop */
	for(;;)
	{
		//    osDelay(1000);
		ddrCycle++;

	   if( get_DDR_isInit() && (SDB0RxMsg == SET) ){
		   User_SDB0_echo();
		   //User_SDB0_bandwidth();
	       SDB0RxMsg = RESET;
	      }

	}

	/* USER CODE END StartTaskDDR */
}

/**
 * @brief  Period elapsed callback in non blocking mode
 * @note   This function is called  when TIM6 interrupt took place, inside
 * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
 * a global variable "uwTick" used as application time base.
 * @param  htim : TIM handle
 * @retval None
 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	/* USER CODE BEGIN Callback 0 */

	/* USER CODE END Callback 0 */
	if (htim->Instance == TIM6) {
		HAL_IncTick();
	}
	/* USER CODE BEGIN Callback 1 */

	/* USER CODE END Callback 1 */
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1)
	{
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
